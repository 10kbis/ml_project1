"""
University of Liege
ELEN0062 - Introduction to machine learning
Project 1 - Classical algorithms
"""
#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

from sklearn.tree import DecisionTreeClassifier

from data import make_data1, make_data2
from plot import plot_boundary


def dt(name, depth, train_x, train_y, test_x, test_y):
    classifier = DecisionTreeClassifier(max_depth=depth)
    classifier.fit(train_x, train_y)
    plot_boundary(name, classifier, test_x, test_y)
    score = classifier.score(test_x, test_y)
    print(name, ':', score)


if __name__ == "__main__":
    data1 = make_data1()
    data2 = make_data2()

    dt("dt_1_1", 1, *data1)
    dt("dt_1_2", 2, *data1)
    dt("dt_1_4", 4, *data1)
    dt("dt_1_8", 8, *data1)
    dt("dt_1_None", None, *data1)

    dt("dt_2_1", 1, *data2)
    dt("dt_2_2", 2, *data2)
    dt("dt_2_4", 4, *data2)
    dt("dt_2_8", 8, *data2)
    dt("dt_2_None", None, *data2)
