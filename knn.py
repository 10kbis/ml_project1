"""
University of Liege
ELEN0062 - Introduction to machine learning
Project 1 - Classical algorithms
"""
#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

from sklearn.neighbors import KNeighborsClassifier

from data import make_data1, make_data2
from plot import plot_boundary


# (Question 2)

# Put your funtions here
# ...


if __name__ == "__main__":
    train_x, train_y, test_x, test_y = make_data1()

    classifier = KNeighborsClassifier()
    classifier.fit(train_x, train_y)
    plot_boundary("knn", classifier, test_x, test_y)
